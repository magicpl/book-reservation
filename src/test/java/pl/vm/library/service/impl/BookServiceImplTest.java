package pl.vm.library.service.impl;

import org.assertj.core.util.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.vm.library.entity.Book;
import pl.vm.library.exception.EntityCannotBeDeletedException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.repository.BookRepository;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.ReservationTo;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookServiceImpl.class)
public class BookServiceImplTest {

    @Autowired
    private BookServiceImpl bookService;

    @MockBean
    private BookRepository bookRepository;

    @MockBean
    private ReservationService reservationService;

    @Test(expected = EntityWithProvidedIdNotFoundException.class)
    public void shouldThrowExceptionWhenBookDoesNotExist() {
        // given
        when(bookRepository.findById(100L)).thenThrow(EntityNotFoundException.class);

        // when
        bookService.delete(100L);

        // then
        verify(bookRepository, times(1)).findById(100L);
        verify(reservationService, never()).findCurrentOrFutureReservationsOnBook(anyLong());

        verify(reservationService, never()).deleteReservationsOnBook(anyLong());
        verify(bookRepository, never()).deleteById(anyLong());
    }

    @Test(expected = EntityCannotBeDeletedException.class)
    public void shouldThrowExceptionWhenCurrentAndFutureReservationsExist() {
        // given
        Book book = createBook();
        book.setId(150L);
        Collection<ReservationTo> reservationsRelatedToBook = createCollectionOfReservations();

        when(bookRepository.findById(150L)).thenReturn(Optional.of(book));
        when(reservationService.findCurrentOrFutureReservationsOnBook(150L)).thenReturn(reservationsRelatedToBook);

        // when
        bookService.delete(150L);

        // then
        verify(bookRepository, times(1)).findById(150L);
        verify(reservationService, times(1)).findCurrentOrFutureReservationsOnBook(anyLong());

        verify(reservationService, never()).deleteReservationsOnBook(anyLong());
        verify(bookRepository, never()).deleteById(anyLong());
    }

    @Test
    public void shouldDeleteReservationsRelatedToBookAndBookWhenNoCurrentOrFutureReservations() {
        // given
        Book book = createBook();

        when(bookRepository.findById(300L)).thenReturn(Optional.of(book));
        when(reservationService.findCurrentOrFutureReservationsOnBook(300L)).thenReturn(Collections.emptySet());

        // when
        bookService.delete(300L);

        // then
        verify(bookRepository, times(1)).findById(300L);
        verify(reservationService, times(1)).findCurrentOrFutureReservationsOnBook(300L);

        verify(reservationService, times(1)).deleteReservationsOnBook(300L);
        verify(bookRepository, times(1)).deleteById(300L);
    }

    private Collection<ReservationTo> createCollectionOfReservations() {
        ReservationTo reservation = createReservationTo();
        reservation.setBookId(150L);
        ReservationTo reservation2 = createReservationTo();
        reservation2.setId(300L);
        reservation2.setBookId(150L);
        return Sets.newLinkedHashSet(reservation, reservation2);
    }

    private Book createBook() {
        Book book = new Book();
        book.setId(150L);
        book.setIsbn("978-3-16-148410-0");
        book.setAuthor("Paulo Coelho");
        book.setTitle("The Devil and Miss Prym");
        book.setReleaseDate(new Date(1558302515L));

        return book;
    }

    private ReservationTo createReservationTo() {
        ReservationTo reservation = new ReservationTo();
        reservation.setId(300L);
        reservation.setBookId(150L);
        reservation.setFromDate(new Date(1550302515L));
        reservation.setToDate(new Date(1558302515L));
        reservation.setUserId(25L);

        return reservation;
    }
}
