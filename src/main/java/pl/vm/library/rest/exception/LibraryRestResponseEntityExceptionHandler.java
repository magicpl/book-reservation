package pl.vm.library.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.vm.library.exception.EntityCannotBeDeletedException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

/**
 * Exception handler for library Rest Controller
 */
@ControllerAdvice
public class LibraryRestResponseEntityExceptionHandler {

    /**
     * Creates response with http status code and message from exception when {@link ParameterValidationException} thrown
     *
     * @param e {@link ParameterValidationException} handled exception
     * @return {@link ResponseEntity} response with {@code HttpStatus.UNPROCESSABLE_ENTITY} http status and exception message
     */
    @ExceptionHandler(ParameterValidationException.class)
    public ResponseEntity handleParameterValidationException(Exception e) {
        return createUnProcessableEntityResponse(e.getMessage());
    }

    /**
     * Creates response with http status code and message from exception when {@link MethodArgumentNotValidException} thrown
     *
     * @param e {@link MethodArgumentNotValidException} handled exception
     * @return {@link ResponseEntity} response with {@code HttpStatus.UNPROCESSABLE_ENTITY} http status and exception message
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleValidationException(MethodArgumentNotValidException e) {
        return createValidationFailedResponse(e);
    }

    /**
     * Creates response with http status code and message from exception when {@link EntityWithProvidedIdNotFoundException} or
     * {@link EntityNotFoundException} thrown
     *
     * @param e {@link MethodArgumentNotValidException} handled exception
     * @return {@link ResponseEntity} response with {@code HttpStatus.NOT_FOUND} http status and exception message
     */
    @ExceptionHandler({EntityWithProvidedIdNotFoundException.class, EntityNotFoundException.class})
    public ResponseEntity handleEntityNotFoundException(RuntimeException e) {
        return createNotFoundResponse(e.getMessage());
    }

    /**
     * Creates response with http status code and message from exception when {@link EntityCannotBeDeletedException} or
     * {@link EntityCannotBeDeletedException} thrown
     *
     * @param e {@link EntityCannotBeDeletedException} handled exception
     * @return {@link ResponseEntity} response with {@code HttpStatus.FORBIDDEN} http status and exception message
     */
    @ExceptionHandler(EntityCannotBeDeletedException.class)
    public ResponseEntity handleEntityCannotBeDeletedException(EntityCannotBeDeletedException e) {
        return createForbiddenResponse(e.getMessage());
    }

    private ResponseEntity createUnProcessableEntityResponse(String message) {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(message);
    }

    private ResponseEntity createValidationFailedResponse(MethodArgumentNotValidException e) {
        String invalidField = Objects.requireNonNull(e.getBindingResult().getFieldError()).getField();
        String validatorMessage = e.getBindingResult().getFieldError().getDefaultMessage();

        String message = "Validation failed on " + invalidField + ".\n" + "Parameter " + validatorMessage;

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(message);
    }

    private ResponseEntity createNotFoundResponse(String message) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }

    private ResponseEntity createForbiddenResponse(String message) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(message);
    }

}
