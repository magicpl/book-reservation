package pl.vm.library.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.ReservationTo;

import javax.validation.Valid;

@RestController
@RequestMapping("/reservations")
public class ReservationRestController {

    @Autowired
    private ReservationService reservationService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ReservationTo saveReservation(@Valid @RequestBody ReservationTo reservationTo) {
        return reservationService.saveReservation(reservationTo);
    }

    @PatchMapping("/extend")
    @ResponseStatus(HttpStatus.OK)
    public ReservationTo extendReservation(@Valid @RequestBody ReservationTo reservationTo) {
        return reservationService.extendReservationPeriod(reservationTo);
    }


}
