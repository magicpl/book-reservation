package pl.vm.library.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vm.library.entity.Book;
import pl.vm.library.exception.EntityCannotBeDeletedException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.repository.BookRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.BookTo;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReservationService reservationService;

    private ModelMapper mapper = new ModelMapper();

    @Override
    public List<BookTo> findAll() {
        List<Book> books = (List<Book>) bookRepository.findAll();

        return books.stream()
                .map(bookEntity -> mapper.map(bookEntity, BookTo.class))
                .collect(Collectors.toList());
    }

    @Override
    public BookTo findById(Long id) {
        return bookRepository.findById(id)
                .map(bookEntity -> mapper.map(bookEntity, BookTo.class))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public BookTo create(BookTo bookTo) {
        validateNewBook(bookTo);

        Book bookEntity = mapper.map(bookTo, Book.class);

        bookRepository.save(bookEntity);

        return mapper.map(bookEntity, BookTo.class);
    }

    @Override
    public void delete(Long id) {

        validateIfBookExists(id);
        validateIfBookIsReserved(id);

        reservationService.deleteReservationsOnBook(id);
        bookRepository.deleteById(id);
    }

    private void validateIfBookExists(Long id) {
        try {
            findById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityWithProvidedIdNotFoundException("Book with provided id does not exist.");
        }
    }

    private void validateIfBookIsReserved(Long bookId) {
        if (CollectionUtils.isNotEmpty(reservationService.findCurrentOrFutureReservationsOnBook(bookId))) {
            throw new EntityCannotBeDeletedException("Book cannot be deleted. There are current or future reservations on this book.");
        }
    }

    private void validateNewBook(BookTo book) {
        if (book.getId() != null) {
            throw new ParameterValidationException("When creating new Book, the ID should be null.");
        }
    }
}
