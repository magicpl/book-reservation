package pl.vm.library.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vm.library.entity.Reservation;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.repository.ReservationRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.service.ReservationService;
import pl.vm.library.service.UserService;
import pl.vm.library.to.ReservationTo;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    private ModelMapper mapper = new ModelMapper();

    @Override
    public ReservationTo saveReservation(ReservationTo reservationTo) {

        validateIfReservationIdIsNotProvided(reservationTo);
        validateIfFromDateIsPresentOrFuture(reservationTo);
        validateIfFromDateIsBeforeToDate(reservationTo);
        validateIfBookExists(reservationTo);
        validateIfUserExists(reservationTo);

        Reservation reservation = mapReservationTOtoReservation(reservationTo);

        validateIfReservationIsConflicted(reservation);

        Reservation savedReservation = reservationRepository.save(reservation);

        return mapReservationToReservationTO(savedReservation);
    }

    @Override
    public ReservationTo extendReservationPeriod(ReservationTo reservationTo) {
        validateIfFromDateIsBeforeToDate(reservationTo);

        Reservation reservation = findReservation(reservationTo);

        validateIfExtendedToDateIsLaterThanCurrentToDate(reservationTo, reservation);
        validateIfToDateIsPresentOrFuture(reservationTo);
        validateIfFromDateNotChanged(reservationTo, reservation);
        validateIfBookNotChanged(reservationTo, reservation);
        validateIfUserNotChanged(reservationTo, reservation);
        validateIfReservationCanBeExtendedToRequestedTime(reservationTo, reservation);

        Reservation reservationToSave = mapReservationTOtoReservation(reservationTo);
        Reservation savedReservation = reservationRepository.save(reservationToSave);

        return mapReservationToReservationTO(savedReservation);
    }

    @Override
    public Collection<ReservationTo> findCurrentOrFutureReservationsOnBook(Long bookId) {
        Collection<Reservation> presentAndFutureReservations = reservationRepository.findAllByBookAndToDateGreaterThanEqualCurrentDate(bookId);
        return presentAndFutureReservations
                .stream()
                .map(reservationEntity -> mapper.map(reservationEntity, ReservationTo.class))
                .collect(Collectors.toSet());
    }

    @Override
    public void deleteReservationsOnBook(Long bookId) {
        reservationRepository.deleteByBookId(bookId);
    }

    private Reservation findReservation(ReservationTo toValidate) {
        return reservationRepository.findById(toValidate.getId())
                .orElseThrow(EntityWithProvidedIdNotFoundException::new);
    }

    private void validateIfReservationIdIsNotProvided(ReservationTo toValidate) {
        if (toValidate.getId() != null) {
            throw new ParameterValidationException("New reservation cannot be saved with id.");
        }
    }

    private void validateIfBookExists(ReservationTo toValidate) {
        try {
            bookService.findById(toValidate.getBookId());
        } catch (EntityNotFoundException e) {
            throw new EntityWithProvidedIdNotFoundException("Book from save reservation request does not exist.");
        }
    }

    private void validateIfUserExists(ReservationTo toValidate) {
        try {
            userService.findById(toValidate.getUserId());
        } catch (EntityWithProvidedIdNotFoundException e) {
            throw new EntityWithProvidedIdNotFoundException("User from save reservation request does not exist.");
        }
    }

    private void validateIfFromDateIsBeforeToDate(ReservationTo toValidate) {
        if (toValidate.getFromDate().after(toValidate.getToDate())) {
            throw new ParameterValidationException("To date cannot be before from date.");
        }
    }

    private void validateIfFromDateIsPresentOrFuture(ReservationTo toValidate) {
        if (isYoungerThanNow(toValidate.getFromDate())) {
            throw new ParameterValidationException("From date cannot be in the past.");
        }
    }

    private void validateIfToDateIsPresentOrFuture(ReservationTo toValidate) {
        if (isYoungerThanNow(toValidate.getToDate())) {
            throw new ParameterValidationException("To date cannot be in the past.");
        }
    }

    private boolean isYoungerThanNow(Date dateToValidate) {
        ZonedDateTime timeToValidate = dateToValidate.toInstant().atZone(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);
        ZonedDateTime now = Instant.now().atZone(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);

        return timeToValidate.isBefore(now);
    }

    private void validateIfReservationIsConflicted(Reservation toValidate) {
        Collection<Reservation> conflictingReservations =
                reservationRepository.findConflictingReservations(toValidate.getBook().getId(), toValidate.getFromDate(), toValidate.getToDate());
        if (CollectionUtils.isNotEmpty(conflictingReservations)) {
            throw new ParameterValidationException("Book is reserved within provided time period.");
        }
    }

    private void validateIfBookNotChanged(ReservationTo toValidate, Reservation currentReservation) {
        if (!toValidate.getBookId().equals(currentReservation.getBook().getId())) {
            throw new ParameterValidationException("Book does not match with book from reservation.");
        }
    }

    private void validateIfUserNotChanged(ReservationTo toValidate, Reservation currentReservation) {
        if (!toValidate.getUserId().equals(currentReservation.getUser().getId())) {
            throw new ParameterValidationException("User does not match with user from reservation.");
        }
    }

    private void validateIfFromDateNotChanged(ReservationTo toValidate, Reservation currentReservation) {
        if (!toValidate.getFromDate().equals(currentReservation.getFromDate())) {
            throw new ParameterValidationException("From date does not match with current reservation.");
        }
    }

    private void validateIfExtendedToDateIsLaterThanCurrentToDate(ReservationTo toValidate, Reservation currentReservation) {
        ZonedDateTime currentFromDate = currentReservation.getToDate().toInstant().atZone(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);
        ZonedDateTime extendedFromDateTimestamp = toValidate.getToDate().toInstant().atZone(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);

        if (currentFromDate.isAfter(extendedFromDateTimestamp) || extendedFromDateTimestamp.equals(currentFromDate)) {
            throw new ParameterValidationException("Extended to date cannot be equal or before current to date");
        }
    }

    private void validateIfReservationCanBeExtendedToRequestedTime(ReservationTo toValidate, Reservation currentReservation) {
        Collection<Reservation> conflictingReservations =
                reservationRepository.findConflictingReservations(currentReservation.getId(), currentReservation.getBook().getId(),
                        currentReservation.getToDate(), toValidate.getToDate());
        if (CollectionUtils.isNotEmpty(conflictingReservations)) {
            throw new ParameterValidationException("Reservation cannot be extended. Book is reserved during requested to date extension.");
        }
    }

    private Reservation mapReservationTOtoReservation(ReservationTo reservationTo) {
        return mapper.map(reservationTo, Reservation.class);
    }

    private ReservationTo mapReservationToReservationTO(Reservation reservation) {
        return mapper.map(reservation, ReservationTo.class);
    }
}
