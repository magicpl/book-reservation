package pl.vm.library.service;

import pl.vm.library.entity.Reservation;
import pl.vm.library.to.ReservationTo;

import java.util.Collection;

/**
 * The Service which contains business logic for Reservation.
 */
public interface ReservationService {

    /**
     * Saves new {@link Reservation} reservation.
     *
     * @param reservation {@link ReservationTo} reservation transport object
     * @return saved reservation
     */
    ReservationTo saveReservation(ReservationTo reservation);

    /**
     * Extends end date of reservation.
     *
     * @param reservation {@link ReservationTo} reservation transport object
     * @return extended reservation
     */
    ReservationTo extendReservationPeriod(ReservationTo reservation);

    /**
     * Returns present or future reservations on provided book.
     *
     * @param bookId {@link Long} id of book related to reservations
     * @return present or future reservations on book
     */
    Collection<ReservationTo> findCurrentOrFutureReservationsOnBook(Long bookId);

    /**
     * Deletes all reservations on book.
     *
     * @param bookId id of book which reservations should be deleted
     */
    void deleteReservationsOnBook(Long bookId);
}
