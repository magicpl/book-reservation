package pl.vm.library.exception;

/**
 * Exception used when trying to delete entity which cannot be deleted
 */
public class EntityCannotBeDeletedException extends RuntimeException {

    public EntityCannotBeDeletedException() {
        super("The Entity cannot be deleted.");
    }

    public EntityCannotBeDeletedException(String message) {
        super(message);
    }
}
