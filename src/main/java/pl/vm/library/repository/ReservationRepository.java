package pl.vm.library.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.vm.library.entity.Reservation;

import java.util.Collection;
import java.util.Date;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {

    /**
     * Finds reservations with the same book id where from date and to date are in between new reservation.
     *
     * @param bookId   id of book corresponding to reservation being tested
     * @param fromDate start date of testing reservation
     * @param toDate   end date of testing reservation
     * @return reservations being in conflict with provided one
     */
    @Query("SELECT reservation FROM Reservation reservation JOIN reservation.book book ON book.id = :bookId " +
            "AND ( " +
            ":fromDate = reservation.fromDate AND :toDate = reservation.toDate " +
            "OR " +
            ":fromDate > reservation.fromDate AND :fromDate < reservation.toDate " +
            "OR " +
            ":fromDate <= reservation.fromDate AND :toDate > reservation.fromDate" +
            ")")
    Collection<Reservation> findConflictingReservations(@Param("bookId") Long bookId,
                                                        @Param("fromDate") Date fromDate,
                                                        @Param("toDate") Date toDate);

    /**
     * Finds reservations with the same book id where from date is before extended to date.
     *
     * @param reservationId  id of reservation which should not be compared
     * @param bookId         id of book corresponding to reservation being tested
     * @param toDate         current end date of reservation (related to reservation with reservationId parameter)
     * @param toDateExtended date after to date parameter, before which reservations are searching
     * @return reservations which start before to date extended parameter
     */
    @Query("SELECT reservation FROM Reservation reservation " +
            "WHERE reservation.id <> :reservationId AND " +
            "reservation.book.id = :bookId AND " +
            "reservation.fromDate BETWEEN :toDate AND :toDateExtended"
    )
    Collection<Reservation> findConflictingReservations(@Param("reservationId") Long reservationId,
                                                        @Param("bookId") Long bookId,
                                                        @Param("toDate") Date toDate,
                                                        @Param("toDateExtended") Date toDateExtended);

    /**
     * Finds all reservations related to given book and to date greater or equal now
     *
     * @param bookId id of book related to reservations
     * @return reservations related to book with to date present or future
     */
    @Query("SELECT reservation FROM Reservation reservation JOIN reservation.book book ON book.id = :bookId " +
            "WHERE reservation.toDate >= current_date")
    Collection<Reservation> findAllByBookAndToDateGreaterThanEqualCurrentDate(Long bookId);

    /**
     * Deletes all reservations related to given book
     *
     * @param bookId id of book which reservations are deleted
     */
    void deleteByBookId(Long bookId);
}
